import os
import csv
from datetime import datetime

def main(dateformat='%Y-%m-%d %H:%M:%S', filename='/Users/portalink/Documents/McCData2.csv'):
    header = True
    count = 0
    output = {}

    file = open(filename)
    csv_data = csv.reader(file)
    print(dateformat)
    print(filename)

    for row in csv_data:
        scope = []
        count += 1
        ord_no = row.pop(0)
        exported_by = row.pop(0)
        has_disc = row.pop(0)
        exp_date = datetime.strptime(row.pop(0), dateformat)

        for item in row:
            try:
                if(item):
                    scope.append(datetime.strptime(item, dateformat))
            except ValueError:
                print('Error in line ' + str(count))
                continue
        try:
            view_date = nearest(scope, exp_date)
        except ValueError:
            print('Error in line ' + str(count))
            view_date = exp_date
        
        expdate = datetime.strftime(exp_date,dateformat)
        viewed = datetime.strftime(view_date,dateformat)
        output.update({count:{
            'ord_no': ord_no,
            'exported_by': exported_by,
            'has_disc': has_disc,
            'exp_date':expdate,
            'last_viewed_date':viewed
        }})


    with open('output2.csv', 'w', newline='') as csvfile:
        fieldnames = ['ord_no', 'exported_by', 'has_disc', 'exp_date', 'last_viewed_date']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if header:
            writer.writeheader
            header = False
        
        for row in output:
            writer.writerow(output[row])

def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))

if __name__ == "__main__":
    main()